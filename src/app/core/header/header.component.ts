import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';

import { DataStorageService } from '../../shared/data-storage.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private dataStorageService: DataStorageService,
    private authService: AuthService) { }

  ngOnInit() { }

  // TODO: Do not use Response here (remove dependency of Http)

  onSaveData() {
    this.dataStorageService.saveData().subscribe(
      (response: Response) => {
        console.log(response);
      },
      (error: Response) => {
        console.log(error);
      }
    );
  }

  onFetchData() {
    this.dataStorageService.fetchData();
  }

  onSignout() {
    console.log('Sign out');
    this.authService.signoutUser();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

}
