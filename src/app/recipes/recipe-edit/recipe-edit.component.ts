import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { Recipe } from '../recipe.model';
import { RecipesService } from '../recipes.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  private id: number;
  private edit = false;

  private recipe: Recipe;

  form: FormGroup;

  constructor(private recipesService: RecipesService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.edit = (params['id'] != null);
        this.recipe = this.recipesService.getRecipe(this.id);
        console.log('Edit mode = ' + this.edit);
        this.initForm();
      }
    );
  }

  private initForm() {
    // default values
    let name = '';
    let imagePath = '';
    let description = '';
    let ingredients = [];
    // edit or create
    if (this.recipe) {
      name = this.recipe.name;
      imagePath = this.recipe.imagePath;
      description = this.recipe.description;
      ingredients = this.recipe.ingredients;
    }
    // build form and array
    // tslint:disable-next-line:prefer-const
    let ingredientsFormArray = new FormArray([]);
    if (ingredients) {
      for (const ingredient of ingredients) {
        ingredientsFormArray.push(
          new FormGroup({
            'name': new FormControl(ingredient.name, Validators.required),
            'amount': new FormControl(ingredient.amount,
              [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
          })
        );
      }
    }
    this.form = new FormGroup({
      'name': new FormControl(name, Validators.required),
      'imagePath': new FormControl(imagePath, Validators.required),
      'description': new FormControl(description, Validators.required),
      'ingredients': ingredientsFormArray
    });
  }

  private clearForm() {
    this.id = undefined;
    this.edit = undefined;
    this.recipe = undefined;
    this.form.reset();
  }

  getIngredientControls() {
    return (<FormArray> this.form.get('ingredients')).controls;
  }

  onSubmit() {
    console.log(this.form);
    // const name = this.form.value['name'];
    // const imagePath = this.form.value['imagePath'];
    // const description = this.form.value['description'];
    // const ingredients = this.form.value['ingredients'];
    // const recipe = new Recipe(name, description, imagePath, ingredients);
    // update or add
    if (this.edit) {
      this.recipesService.updateRecipe(this.id, this.form.value);
    } else {
      this.recipesService.addRecipe(this.form.value);
    }
    this.clearForm();
    this.router.navigate(['../'], { relativeTo: this.route }); // back
  }

  onCancel() {
    this.clearForm();
    this.router.navigate(['../'], { relativeTo: this.route }); // back
  }

  onAddIngredient() {
    (<FormArray> this.form.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null,
          [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  onDeleteIngredient(id: number) {
    (<FormArray> this.form.get('ingredients')).removeAt(id);
  }

}
