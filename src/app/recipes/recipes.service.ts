import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  recipesChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe('Crispy Bacon Burguer', 'Burguer with crispy bacon',
      // tslint:disable-next-line:max-line-length
      'https://images.unsplash.com/photo-1432752641289-a25fc853fceb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
      [new Ingredient('bacon', 2)]),
      new Recipe('Bacon Cheese Burguer', 'Burguer with cheese and bacon',
      // tslint:disable-next-line:max-line-length
      'https://images.unsplash.com/photo-1498579485796-98be3abc076e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
      [new Ingredient('cheese', 1),
      new Ingredient('bacon', 1)]),
  ];

  constructor(private shoppingListService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice(); // copy of the array
  }

  getRecipe(id: number) {
    return this.recipes[id]; // id is the array index
  }

  addRecipe(recipe: Recipe) {
    console.log(recipe);
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(id: number, recipe: Recipe) {
    console.log(recipe);
    this.recipes[id] = recipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(id: number) {
    this.recipes.splice(id, 1); // remove 1 element on index position
    this.recipesChanged.next(this.recipes.slice());
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    console.log(ingredients);
    this.shoppingListService.addIngredients(ingredients);
  }

}
