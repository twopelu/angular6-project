import { Injectable } from '@angular/core';
import { CanActivate,  CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route, Router, UrlSegment } from '@angular/router';

import { AuthService } from './auth.service';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  // TODO: Redirect to next page after login success

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canActivate');
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['signin']);
    }
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canLoad');
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['signin']);
    }
  }

}
