import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // TODO: Check for existing token in the app initialization

  // TODO: Redirect user to signin when accesing to protected route

  // TODO: Disable buttons New Recipe and Edit Recipe in templates

  token: string;

  constructor(private router: Router) {}

  signupUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(
      value => {
        console.log(value);
      }
    )
    .catch(
      error => {
        console.log(error);
      }
    );
  }

  signinUser(email: string, password: string) {
    firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, password)
    .then(
      value => {
        console.log(value);
        this.router.navigate(['/']); // go to home
        firebase.auth().currentUser.getIdToken()
        .then(
          (token: string) => {
            this.token = token;
          }
        );
      }
    )
    .catch(
      error => {
        console.log(error);
      }
    );
  }

  getToken() {
    firebase.auth().currentUser.getIdToken()
    .then(
      (token: string) => {
        this.token = token;
      }
    );
    return this.token; // token may be expired
  }

  isAuthenticated() {
    return (this.token != null);
  }

  signoutUser() {
    firebase.auth().signOut();
    this.token = null;
    this.router.navigate(['/']); // go to home
  }

}
