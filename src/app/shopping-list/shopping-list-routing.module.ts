import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListEditComponent } from './shopping-list-edit/shopping-list-edit.component';

const shoppingListRoutes: Routes = [
  { path: '', component: ShoppingListComponent, children: [
    { path: 'edit/:id', component: ShoppingListEditComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(shoppingListRoutes)],
  exports: [RouterModule]
})
export class ShoppingListRoutingModule { }
