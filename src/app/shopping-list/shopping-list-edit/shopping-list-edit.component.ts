import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm} from '@angular/forms';

import { Subscription } from 'rxjs';

import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {

  @ViewChild('form') shoppingListForm: NgForm;

  private subscription: Subscription;

  id: number;
  edit = false;

  ingredient: Ingredient;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.shoppingListService.ingredientEdit.subscribe(
      (id: number) => {
        this.id = id;
        this.edit = true;
        this.ingredient = this.shoppingListService.getIngredient(id);
        this.shoppingListForm.setValue({
            name: this.ingredient.name,
            amount: this.ingredient.amount
          });
        console.log('Edit ingredient ' + this.ingredient);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit(form: NgForm) {
    console.log(form);
    const name = form.value.name;
    const amount = form.value.amount;
    const ingredient = new Ingredient(name, amount);
    // update or add
    if (this.edit) {
      this.shoppingListService.updateIngredient(this.id, ingredient);
    } else {
      this.shoppingListService.addIngredient(ingredient);
    }
    this.clearForm();
  }

  onClear() {
    this.clearForm();
  }

  onDelete() {
    this.shoppingListService.deleteIngredient(this.id);
    this.clearForm();
  }

  private clearForm() {
    this.id = undefined;
    this.edit = false;
    this.shoppingListForm.reset();
  }

}
