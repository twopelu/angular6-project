import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  ingredientsChanged = new Subject<Ingredient[]>();

  ingredientEdit = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomato', 10)
  ];

  constructor() { }

  getIngredients() {
    return this.ingredients.slice(); // copy of the array
  }

  getIngredient(id: number) {
    return this.ingredients[id]; // id is the array index
  }

  // TODO group ingredients with the same name

  addIngredients(ingredients: Ingredient[]) {
    console.log(ingredients);
    this.ingredients.push(...ingredients); // spread operator (ES6)
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  addIngredient(ingredient: Ingredient) {
    console.log(ingredient);
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  updateIngredient(id: number, ingredient: Ingredient) {
    console.log(ingredient);
    this.ingredients[id] = ingredient;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  deleteIngredient(id: number) {
    this.ingredients.splice(id, 1); // remove 1 element on index position
    this.ingredientsChanged.next(this.ingredients.slice());
  }

}
