import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  // tslint:disable-next-line:no-inferrable-types
  @HostBinding('class.open') isOpen: boolean = false;

  @HostListener('click') toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

}
