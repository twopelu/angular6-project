import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { map } from 'rxjs/operators';

import { RecipesService } from '../recipes/recipes.service';
import { Recipe } from '../recipes/recipe.model';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: Http, private recipesService: RecipesService,
    private authService: AuthService) { }

  saveData() {
    const token = this.authService.getToken();
    return this.http.put('https://angular6-project-bd3a8.firebaseio.com/recipes.json?auth=' + token,
      this.recipesService.getRecipes(),
      ); // observable
  }

  fetchData() {
    const token = this.authService.getToken();
    return this.http.get('https://angular6-project-bd3a8.firebaseio.com/recipes.json?auth=' + token)
    .pipe(map(
      (response: Response) => {
        const recipes: Recipe[] = response.json();
        // tslint:disable-next-line:prefer-const
        for (let recipe of recipes) {
          if (!recipe['ingredients']) {
            recipe['ingredients'] = [];
          }
        }
        return recipes;
      }
    ))
    .subscribe(
      (recipes: Recipe[]) => {
        console.log(recipes);
        this.recipesService.setRecipes(recipes);
      },
      (error: Response) => {
        console.log(error);
      }
    );
  }

}
